﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KamikazeTestStuffProject {
    class ClassEventTest {
        public delegate void TestDelegateEventHandler(object source, EventArgs args);
        public event TestDelegateEventHandler Test1Done;

        public void Test1_EventHandler() {
            //OnTest1Done();
            Test1Done?.Invoke(this, EventArgs.Empty); // Worse coding style but equally valid instead of calling a separate function
        }

        protected virtual void OnTest1Done() {
            Test1Done?.Invoke(this, EventArgs.Empty);
        }
    }
}
