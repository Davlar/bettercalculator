﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KamikazeTestStuffProject {
    public class ClassRunTests {
        //public delegate void WriteTextEventHandler(object source, TextEventArgs args); // Sets up event specification
        public delegate void WriteTextEventHandler(string text); // Sets up event specification
        public event WriteTextEventHandler WriteText; // Sets up event instance

        private string m_message = "";

        public void EventTest() {
            var cl = new ClassEventTest();
            cl.Test1Done += OnTest1Done; // bind as subscriber
            cl.Test1_EventHandler();
        }

        public void LowerTest() {
            Func<string, string> function = ClassDelegateTest.LowerCase;
            var textToChange = "NIssEAESss";
            m_message += textToChange + " => " + ClassDelegateTest.ActionTest(textToChange, function);
            OnWriteText(); // Calls Event
        }

        public void OnTest1Done(object source, EventArgs e) {
            m_message = "Test1 done";
            OnWriteText(); // Calls Event but could just be called on the line aswell = WriteText?.Invoke(m_message);
        }

        protected virtual void OnWriteText() {
            WriteText?.Invoke(m_message);
        }
    }
}
