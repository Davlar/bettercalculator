﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KamikazeTestStuffProject {
    public class ClassDelegateTest {        

        public static string ActionTest(string text, Func<string,string> function) {
            var newText = function(text);
            return newText;
        } 

        public static string LowerCase(string input) {
            return input.ToLower();
        }
    }
}
