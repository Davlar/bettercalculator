using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests_LibraryOctaveish {
    /* Remember!
     * Arrange 
     * Act
     * Assert
     */

    [TestClass]
    public class Test_BinaryFunctions
    {
        [TestMethod]
        [DataRow(8, "1000")]
        [DataRow(67123, "10000011000110011")]
        [DataRow(3445029135783676, "1100001111010011110001011001111000010001011011111100")]
        [DataRow(-7, "1001")]
        public void Dec2Bin_CorrectInputs_AssertEqual(Int64 input, string expected)
        {
            var MF = new LibraryOctaveisch.ClassBinaryFunctions();
            var result = MF.Dec2Bin(input);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [DataRow(8, 8, "00001000")]
        [DataRow(67123, 17, "10000011000110011")]
        [DataRow(3445029135783676, 54, "001100001111010011110001011001111000010001011011111100")]
        [DataRow(-7, 6, "111001")]
        public void Dec2Bin_WithLen_CorrectInputs_AssertEqual(Int64 input, int length, string expected) {
            var MF = new LibraryOctaveisch.ClassBinaryFunctions();
            var result = MF.Dec2Bin(input, length);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [DataRow("1000", 8)]
        [DataRow("10000011000110011", 67123)]
        [DataRow("1100001111010011110001011001111000010001011011111100", 3445029135783676)]
        public void Bin2Dec_CorrectInputs_AssertEqual(string input, Int64 expected) {
            var MF = new LibraryOctaveisch.ClassBinaryFunctions();
            var result = MF.Bin2Dec(input);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [DataRow("1001", -7)]
        [DataRow("111111101111100111001101", -67123)]
        [DataRow("10011110000101100001110100110000111101110100100000100", -3445029135783676)]
        public void Bin2Dec_NegativeInputs_AssertEqual(string input, Int64 expected) {
            var MF = new LibraryOctaveisch.ClassBinaryFunctions();
            var result = MF.Bin2Dec(input, true);
            Assert.AreEqual(expected, result);
        }

    }
}
