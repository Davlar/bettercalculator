﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Useful line: throw new NotImplementedException("This function:" + System.Reflection.MethodBase.GetCurrentMethod().Name + " is not implemented");

namespace LibraryOctaveisch {
    public class ClassBinaryFunctions {
        public string Dec2Bin(Int64 input) {
            string result = "0";
            if (input > 0) {
                result = Convert.ToString(input, 2);
            }
            else if (input < 0) {
                var len = Convert.ToInt32(Math.Log(input*-1) / Math.Log(2))+1; // Add extra bit that is truncated instead of rounding
                result = Convert.ToString(input, 2);
                result = result.Substring(result.Length - len, len);
            }

            return result;         
        }

        public string Dec2Bin(Int64 input, int len) {
            string appendVal = (input < 0) ? "1" : "0";
            StringBuilder result = new StringBuilder();
            result.Append(Dec2Bin(input));

            if (result.Length > len) { 
                throw new ArgumentOutOfRangeException("Length can't be shorter then binary numbers need to represent it in: " + System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            result.Insert(0, appendVal, len - result.Length);
            return result.ToString();
        }

        public Int64 Bin2Dec(string bin) {
            var result = Convert.ToInt64(bin, 2);
            return result;
            throw new NotImplementedException("This function:" + System.Reflection.MethodBase.GetCurrentMethod().Name + " is not implemented");
        }

        public Int64 Bin2Dec(string bin, bool isNegative) {
            Int64 result = 0;
            Int64 mask = 1;
            result = Bin2Dec(bin);

            if (isNegative) {
                for (var i = 0; i < bin.Length-1; i++)
                    mask = (mask << 1) + 1;

                result = (~(result) & mask);
                result = ((result + 1) * -1);
            }
            
            return result;
            throw new NotImplementedException("This function:" + System.Reflection.MethodBase.GetCurrentMethod().Name + " is not implemented");
        }
    }
}
