﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryOctaveisch {
    public class ClassLookupFunction {
        private enum AvailableFunctions {
            bin2dec = 0,
            dec2bin = 1,
        }

        private ClassBinaryFunctions binFunc = new ClassBinaryFunctions();


        public void StringToFunction(string input) {
            AvailableFunctions functionEnum;
            input = input.ToLower();


            foreach (string name in Enum.GetNames(typeof(AvailableFunctions))) {
                if (input.Contains(name)) { // Finds functions
                    if (Enum.TryParse(name, out functionEnum)) {
                        Debug.WriteLine("Could find the function " + name + " with number: " + Convert.ToInt32(functionEnum));
                    }
                }
            }

        }

    }

}

