﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using KamikazeTestStuffProject;
using LibraryOctaveisch;

namespace BetterCalculator {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        TestingClass TC = new TestingClass();
        ClassLookupFunction CLF = new ClassLookupFunction();

        public MainWindow() {
            //if (Debugger.IsAttached)
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo("en-US");

            InitializeComponent();
        }

        private void SwitchButton_Click(object sender, RoutedEventArgs e) {
            SwitchTab();
        }

        private void AddTabButton_Click(object sender, RoutedEventArgs e) {
            AddTab();
        }

        private void TestButton_Click(object sender, RoutedEventArgs e) {
            TC.TestCommand(this);
        }

        private void SwitchTab() {
            tabControl.SelectedIndex = tabControl.SelectedIndex + 1;
            PrintToCommandBox(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        private void AddTab() {
            tabControl.Items.Add(new TabItem { Header = "Test", Name = "Test" });
            PrintToCommandBox(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        public void PrintToCommandBox(string message) {
            textBoxCommandHistory.Text += message + "\n";
        }

        private void textBoxCommand_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Enter) {
                PrintToCommandBox(textBoxCommand.Text);
                CLF.StringToFunction(textBoxCommand.Text);
                textBoxCommand.Clear();
            }

        }


    }

    public class TestingClass {
        public void TestCommand(MainWindow mw) {
            var cl = new ClassRunTests();
            cl.WriteText += mw.PrintToCommandBox;
            cl.LowerTest();
            cl.EventTest();
        }
    }
}
